---
title: "Hello SVG PORN"
date: 2020-02-22T14:02:50+09:00
description: "たくさんのSVGがまとめられているサイト"
draft: false
tags: [Hello, "SVG PORN"]
---

[SVG PORN](https://svgporn.com)は、ハイクオリティなSVGのロゴがまとめられているサイトです。
ライセンスは`Creative Commons Zero v1.0 Universal`で誰でも自由に使うことができます。
[前回の記事](../hello-site)でも使っています。
サービスのロゴから、ブラウザやツール、プログラミング言語のロゴも、バリエーション豊かなロゴがたくさんあります。
