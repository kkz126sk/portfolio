---
title: "Hello Site"
date: 2020-02-21T20:15:41+09:00
description: "このサイトの紹介です。"
draft: false
tags: [Hello, 技術紹介]
---

## このサイトは？

自分が得た情報を外部にアウトプットするサイトです。

## このサイトで使われている技術

このサイトはMITライセンスです。
リポジトリは[こちら](https://gitlab.com/kkz126sk/portfolio)です。

### [Hugo](https://gohugo.io)

![](/images/hugo.svg)

Hugoはスタティックサイトジェネレーターのひとつで、Markdownで書いた記事をHTMLに変換できます。
競合に、[Hexo](https://hexo.io)や[Jekyll](https://jekyllrb.com)などがあります。

### [Netlify](https://netlify.com)

![](/images/netlify.svg)

Netlifyはスタティックサイトのホスティングサービスです。
競合に、Firebase Hostingというものがありますが、使ったことがないです。
Gitリポジトリを使ったホスティングでは、Github Pagesや、GitLab Pagesがあります。
Netlifyは他のGitホスティングサービスと連携できて、操作が楽です。

## ドメイン

### [freenom.com](https://freenom.com)

freenomは無料でドメインを取得できるサービスです。
有料でもドメインを買うことができます。

## Git ホスティングサービス

### [GitLab](https://gitlab.com)

![](/images/gitlab.svg)

GitLabはGithubより後発のGitホスティングサービスです。
Googleアカウントなど、各種サービスからのログインに対応しています。
もちろん、メールアドレスでの登録も可能です。
競合に、[Github](https://github.com)や[Bitbucket](https://bitbucket.org)があります。
また、GitLabはローカルで動かすこともできます。
