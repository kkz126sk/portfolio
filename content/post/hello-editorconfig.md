---
title: "Hello Editorconfig"
date: 2020-02-22T14:50:47+09:00
description: "Editorconfigの紹介"
draft: false
tags: [Hello, EditorConfig]
---

## Editorconfig?

Editorconfigは、ひとつのファイルでコーディングスタイルを設定できるツールです。
使うには、まず使うエディターにEditorconfigのプラグインをインストールします。
元からEditorconfigに対応しているエディターもあるので、確認してみてください。

## 各エディターへのプラグインのインストール方法

### Visual Studio Code

`Extensions`から、`editorconfig`を検索し、インストール。

## 設定ファイル

グローバルな設定ファイルは`~/.editorconfig`です。ローカルな設定ファイルは`.editorconfig`でプロジェクトのルートディレクトリに置いてください。

### Quick Start

```
git clone https://github.com/editorconfig/editorconfig
cp editorconfig/.editorconfig ~
```

ホームディレクトリに`.editorconfig`をコピーします。

### .editorconfig

```
#editorconfig.org
root = true

[*]
indent_style = space
indent_size = 2
end_of_line = lf
charset = utf-8
trim_trailing_whitespace = true
insert_trailing_newline = true

[*.md]
trim_trailing_whitespace = false

```

TOML形式の設定ファイルです。

#### インデントスタイル

ソフトタブにしたい！

```
indent_style = space
```

ハードタブにしたい！

```
indent_style = tab
```

#### インデントサイズ

インデントサイズを`2`にしたい！

```
indent_size = 2
```

#### 改行コード

改行コードを`LF`にしたい！（`LF`は主にLinuxで使われている改行コード）

```
end_of_line = lf
```

改行コードを`CRLF`にしたい！（`CRLF`は主にWindowsで使われている改行コード）

```
end_of_line = crlf
```

#### 文字コード

`UTF-8`を使いたい！

```
charset = utf-8
```

#### 行末のスペースを削除する

```
trim_trailing_whitespace = true
```

削除しない

```
trim_trailing_whitespace = false
```

#### 最終行に改行を加える

```
insert_trailing_newline = true
```

加えない

```
insert_trailing_newline = false
```

#### さらに外側の`.editorconfig`を検索しない

```
root = true
```

検索する

```
root = false
```
